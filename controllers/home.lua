--[[
          _                     _     _
         | |   _   _  __ _  ___| |__ (_)
         | |  | | | |/ _` |/ __| '_ \| |
         | |__| |_| | (_| | (__| | | | |
         |_____\__,_|\__,_|\___|_| |_|_|
Copyright (c) 2020  Díaz  Víctor  aka  (Máster Vitronic)
<vitronic2@gmail.com>   <mastervitronic@vitronic.com.ve>
]]--
local home	=	class('home');

function home:show_main()
	view:add_content('title',"Luachi | Home")
	view:add_content('description',"Luachi, un framework web para Lua")
	view:add_contents({
		js = {
			("/js/themes/%s/common/jquery.js"):format(conf.theme.theme),
			("/js/themes/%s/common/script.js"):format(conf.theme.theme),
			("/js/themes/%s/public/home/home.js"):format(conf.theme.theme)
		},
		css  = {
			("/css/themes/%s/common/mustard-ui.min.css"):format(conf.theme.theme),
			("/css/themes/%s/common/style.css"):format(conf.theme.theme),
			("/css/themes/%s/public/home/home.css"):format(conf.theme.theme)
		}
	})
	local page   = template.new(
		"/public/home/home.html",
		"/public/page.html"
	)
	view:generate(page)
end

function home:execute()
	if (ENV.REQUEST_METHOD == 'POST') then
		if ( POST['logOut'] == 'true' ) then
			local result = auth:logOut()
			if ( result == true) then
				http:header("Content-type: application/json; charset=utf-8",200)
				print(json:encode({ok=result}))
			end
			return
		end
	end
	self:show_main()
end

return home
